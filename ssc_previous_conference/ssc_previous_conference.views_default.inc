<?php
/**
 * @file
 * ssc_previous_conference.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ssc_previous_conference_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'previous_conferences';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Previous Conferences';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Past Conferences';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['class'] = 'small-block-grid-3 previous-conferences';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Program */
  $handler->display->display_options['fields']['field_conference_program']['id'] = 'field_conference_program';
  $handler->display->display_options['fields']['field_conference_program']['table'] = 'field_data_field_conference_program';
  $handler->display->display_options['fields']['field_conference_program']['field'] = 'field_conference_program';
  $handler->display->display_options['fields']['field_conference_program']['label'] = '';
  $handler->display->display_options['fields']['field_conference_program']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_conference_program']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_conference_program']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_conference_program']['type'] = 'file_url_plain';
  /* Field: Content: Flyer */
  $handler->display->display_options['fields']['field_conference_flyer']['id'] = 'field_conference_flyer';
  $handler->display->display_options['fields']['field_conference_flyer']['table'] = 'field_data_field_conference_flyer';
  $handler->display->display_options['fields']['field_conference_flyer']['field'] = 'field_conference_flyer';
  $handler->display->display_options['fields']['field_conference_flyer']['label'] = '';
  $handler->display->display_options['fields']['field_conference_flyer']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_conference_flyer']['alter']['path'] = '[field_conference_program]';
  $handler->display->display_options['fields']['field_conference_flyer']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_conference_flyer']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_conference_flyer']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_conference_flyer']['settings'] = array(
    'image_style' => 'conference_flyer',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_conference_program]';
  $handler->display->display_options['fields']['title']['element_type'] = '0';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Conference Date */
  $handler->display->display_options['fields']['field_conference_date']['id'] = 'field_conference_date';
  $handler->display->display_options['fields']['field_conference_date']['table'] = 'field_data_field_conference_date';
  $handler->display->display_options['fields']['field_conference_date']['field'] = 'field_conference_date';
  $handler->display->display_options['fields']['field_conference_date']['label'] = '';
  $handler->display->display_options['fields']['field_conference_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_conference_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Sort criterion: Content: Conference Date (field_conference_date) */
  $handler->display->display_options['sorts']['field_conference_date_value']['id'] = 'field_conference_date_value';
  $handler->display->display_options['sorts']['field_conference_date_value']['table'] = 'field_data_field_conference_date';
  $handler->display->display_options['sorts']['field_conference_date_value']['field'] = 'field_conference_date_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'previous_conferences' => 'previous_conferences',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Conference Date (field_conference_date) */
  $handler->display->display_options['sorts']['field_conference_date_value']['id'] = 'field_conference_date_value';
  $handler->display->display_options['sorts']['field_conference_date_value']['table'] = 'field_data_field_conference_date';
  $handler->display->display_options['sorts']['field_conference_date_value']['field'] = 'field_conference_date_value';
  $handler->display->display_options['sorts']['field_conference_date_value']['order'] = 'DESC';
  $handler->display->display_options['path'] = 'about/past-conferences';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Past Conferences';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $export['previous_conferences'] = $view;

  return $export;
}

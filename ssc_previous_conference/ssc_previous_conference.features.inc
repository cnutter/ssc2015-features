<?php
/**
 * @file
 * ssc_previous_conference.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ssc_previous_conference_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function ssc_previous_conference_image_default_styles() {
  $styles = array();

  // Exported image style: conference_flyer.
  $styles['conference_flyer'] = array(
    'name' => 'conference_flyer',
    'label' => 'conference_flyer',
    'effects' => array(
      6 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 530,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function ssc_previous_conference_node_info() {
  $items = array(
    'previous_conferences' => array(
      'name' => t('Previous Conferences'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

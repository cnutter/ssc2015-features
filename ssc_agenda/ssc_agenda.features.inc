<?php
/**
 * @file
 * ssc_agenda.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ssc_agenda_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ssc_agenda_node_info() {
  $items = array(
    'agenda_item' => array(
      'name' => t('Agenda item'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Time'),
      'help' => '',
    ),
  );
  return $items;
}

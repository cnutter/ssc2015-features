<?php
/**
 * @file
 * ssc_clue.features.inc
 */

/**
 * Implements hook_node_info().
 */
function ssc_clue_node_info() {
  $items = array(
    'clue' => array(
      'name' => t('Clue'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
